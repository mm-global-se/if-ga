# Google Analytics

---

[TOC]

## Current Version

1.0.1

## Overview
This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to Google Analytics.

###How we send the data
Our Google Analytics Integration populates a custom dimension with the relevant campaign experience information. For multiple campaigns, the data that is sent across is concatenated so that we do not overwrite any previous campaign data that was sent to the custom variable.

We recommend setting the custom variable scope to "Visitor" level, and where possible to use different custom variables for each test.



###Data Format 
The data sent to Google Analytics will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
) 

By default on each page the data is concatenated for multiple campaigns so to stop each campaign data replacing previous data that was sent. The data is capped at 128 characters, as set by Google Analytics. If the collective campaign data surpasses the character limit then the first generated campaign gets taken out of the sent data.This process is repeated until the data being sent is below 128 characters.

##Prerequisite
The following information needs to be provided by the client: 

+ Campaign Name

+ Account ID (only to be used if advised by the client or the client services team)

+ Custom Variable/Slot 


## Download

* [ga-register.js](https://bitbucket.org/mm-global-se/if-ga/src/master/src/ga-register.js)

* [ga-initialize.js](https://bitbucket.org/mm-global-se/if-ga/src/master/src/ga-initialize.js)


## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [ga-register.js](https://bitbucket.org/mm-global-se/if-ga/src/master/src/ga-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [ga-initialize.js](https://bitbucket.org/mm-global-se/if-ga/src/master/src/ga-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

* Download [Chrome Extension](https://chrome.google.com/webstore/detail/google-analytics-debugger/jnkmfdileelhofjcijamephohjechhna)

* Go to the test page

* Open Chrome DevTools javascript console

* Enable the plugin to capture the data sent to Google Analytics

    ![ua_0.png](https://bitbucket.org/repo/xyEEop/images/1134390230-ua_0.png)

* Look for your campaign related data in console

    ![ga_0.png](https://bitbucket.org/repo/4yzyAq/images/3989194690-ga_0.png)

* Check if the data passed to the custom variable matches Integration Factory [output format](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction)

* Check if multiple experienses are beening correctly collected (concatenated in one string, keeping in mind the character limit)

* Check the `_utm*` cookies are only set against one domain in campaigns run on subdomain

* Traffic source changing in `_utmz` cookie

    1. Clear GA cookies
     
    2. Access site from Google

    3. Check `utmcsr` parameter in `_utmz` cookie - should say `organic`

    4. Navigate to campaign page and generate campaign

    5. Check `utmcsr` parameter - should still be `organic`

* Make sure that GA __Account ID__ is consistent across the site:
    
    1. Check the GA Account ID on the page that doesn't have any campaigns mapped to it (use debugger plugin)

    2. Navigate to campaign page and generate a campaign

    3. Check GA Account ID is the same

* Make sure that GA __Visitor ID__ is consistent across the site:
    
    1. Check the GA Visitor ID on the page that doesn't have any campaigns mapped to it (use debugger plugin)

    2. Navigate to campaign page and generate a campaign

    3. Check GA Visitor ID is the same

## Common Problem Solutions

### Cookie duplication for subdomains

Stop setting account ID from `initialize` script

### Traffic source changing in `_utmz` cookie

Stop setting account ID from `initialize` script