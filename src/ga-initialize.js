mmcore.IntegrationFactory.initialize('Google Analytics', {
  campaign: 'campaign name',
  account: 'account ID', // optional parameter, only to be used if the site/our campaigns are not set on sub-domains. If sub-domains are used please delete this line.
  slot: slot number,
  redirect: false,
  persist:true,
  callback: function () {}
});