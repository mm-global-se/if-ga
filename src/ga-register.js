// version 1.0.1
mmcore.IntegrationFactory.register('Google Analytics', {
    defaults: {
        modifiers: {     
            sitePersist: function (data) {
                /**
                 * modifier configuration
                 * Please adapt to match your case
                 */
                var config = {
                    // allowed amount of characters to be sent to 3d party
                    maxDataSize: 120,
                    // cookie name for campaign data storage
                    cookieName: 'mm-if-site-persist-ga-' + data.slot
                },

                getCampaignIndex=function(a){var b;for(b=a.length;b--;)if(new RegExp("("+data.campaign+")=").test(a[b]))return b;return-1},concatCampaigns=function(){var a=getFromStorage(config.cookieName),b=a?JSON.parse(a):[],c=getCampaignIndex(b);return-1!==c?b[c]=data.campaignInfo:b.push(data.campaignInfo),b.join("&")},getFromStorage=function(){return mmcore.GetCookie(config.cookieName,1)},saveToStorage=function(a){var b=a.split("&");mmcore.SetCookie(config.cookieName,JSON.stringify(b),365,1)},removeOldestCampaigns=function(a){for(var b=a.split("&");;){if(!(b.join("&").length>=config.maxDataSize))return b.join("&");b.shift()}},updateCampaignInfo=function(a){data.currentCampaignInfo=data.campaignInfo,data.campaignInfo=a},initialize=function(){var a=concatCampaigns();a=removeOldestCampaigns(a),updateCampaignInfo(a),saveToStorage(a)}();
            }   
        },
        isStopOnDocEnd: false
    },
    validate: function (data) {
        if(!data.campaign)
          return 'No campaign.';
        if(!data.slot)
          return 'No custom variable slot.';
        if(data.slot < 1 || data.slot > 50 || isNaN(data.slot))
          return 'Invalid custom variable slot. Must be 1-50.';
        return true;
    },
    check: function (data) {
        var ga = window[data.gaVariable || '_gaq'];
        return ga && ga.push;
    },
    exec: function (data)  {
        var prodSand = data.isProduction ? 'MM_Prod_' : 'MM_Sand_',
            ga = window[data.gaVariable || '_gaq'],
            namespace = '';

        if (data.account) {
            namespace = 'mm_' + data.account + '.';
            ga.push([namespace + '_setAccount', data.account]);
        }

        ga.push(
            [namespace + '_setCustomVar', data.slot, prodSand, data.campaignInfo, 1],
            [namespace + '_trackEvent', prodSand, data.campaignInfo, data.campaignInfo, undefined, true]
        );

        if (typeof data.callback === 'function') data.callback();
        return true;
    }
});